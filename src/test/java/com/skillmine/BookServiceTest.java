package com.skillmine;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.skillmine.model.Book;
import com.skillmine.repository.BookRepository;
import com.skillmine.serviceimpl.BookServiceImpl;
@RunWith(MockitoJUnitRunner.class)
public class BookServiceTest {
	@Mock
	BookRepository bookRepository;
	@InjectMocks
	BookServiceImpl service;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
		ReflectionTestUtils.setField(service, "bookRepository", bookRepository);
	}
	@Test
	public void getAllBooksTest() {
		List<Book> list = new ArrayList<Book>();
		list.add(new Book(new Long(10001), "The Monk who sold his ferrari", "Robin Shanrma", "Fiction", 225.00));
		list.add(new Book(new Long(10002), "Inner Engineering", "Sadguru", "Spritual", 199.00));
		list.add(new Book(new Long(10003), "Miracle Morning", "Miracle Morning", "Non Fiction", 123.45));
		list.add(new Book(new Long(10004), "The Power od subconscious Mind", "Joseph M", "Fiction", 250));
		list.add(new Book(new Long(10005), "The Mastry Manual", "Robin Sharma", "Non Fiction", 250.50));

		when(bookRepository.findAll()).thenReturn(list);
		// test
		List<Book> empList = service.getBooks();
		assertEquals(5, empList.size());
		verify(bookRepository, times(1)).findAll();
	}
	
	@Test
	public void getBooksByIdTest() {
	
		Optional<Book> b=Optional.of(new Book(new Long(10001), "The Monk who sold his ferrari", "Robin Shanrma", "Fiction", 225.00));
		when(bookRepository.findById(new Long(10001))).thenReturn(b);
		Book emp = bookRepository.findById(new Long(10001)).get();
		assertEquals("The Monk who sold his ferrari", emp.getTitle());
		assertEquals("Robin Shanrma", emp.getAuther());
		assertEquals("Fiction", emp.getType());
	}

	@Test
	public void createBookTest() {
		Book book = new Book(new Long(10001), "The Monk who sold his ferrari", "Robin Shanrma", "Fiction", 225.00);
		bookRepository.save(book);
		verify(bookRepository, times(1)).save(book);
	}
	
	@Test
	public void deleteBookTest() {
		bookRepository.deleteById(new Long(10001));
		verify(bookRepository, times(1)).deleteById(new Long(10001));
	}
	@Test
	public void updateBookeTest() {
		Book book = new Book(new Long(10001), "The Monk who sold his ferrari", "Robin Shanrma", "Fiction", 225.00);
		bookRepository.save(book);
		verify(bookRepository, times(1)).save(book);
	}
	 
}