package com.skillmine.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import javax.validation.Valid;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.skillmine.AbstractTest;
import com.skillmine.model.Book;
import com.skillmine.serviceimpl.BookServiceImpl;

public class BookControllerTest extends AbstractTest {
	@Override
	@Before
	public void setUp() {
		super.setUp();
	}

	@Test
	public void getBooksListTest() throws Exception {
		String uri = "/books/getAll";
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
		String content = mvcResult.getResponse().getContentAsString();
		Book[] Booklist = super.mapFromJson(content, Book[].class);
		assertTrue(Booklist.length > 0);
	}
	
	@Test(expected = Exception.class)
	public void searchTest() throws Exception {
		String uri = "/search/Robin";
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
		int status = mvcResult.getResponse().getStatus();
		assertEquals(404, status);
		String content = mvcResult.getResponse().getContentAsString();
		Book[] Booklist = super.mapFromJson(content, Book[].class);
		assertTrue(Booklist.length > 0);
	}
	
	@Test
	public void getBookTest() throws Exception {
		String uri = "/books/getAll";
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
		String content = mvcResult.getResponse().getContentAsString();
		Book[] Booklist = super.mapFromJson(content, Book[].class);
		assertTrue(Booklist.length > 0);
	}
	@Test
	public void createBook() throws Exception {
		String uri = "/books/create";
		Book Book = new Book(new Long(10001), "The Monk who sold his ferrari", "Robin Shanrma", "Fiction", 225.00);
		String inputJson = super.mapToJson(Book);
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
				.andReturn();
		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
		String content = mvcResult.getResponse().getContentAsString();
		assertEquals(content, "{\"isbn\":7,\"title\":\"The Monk who sold his ferrari\",\"auther\":\"Robin Shanrma\",\"type\":\"Fiction\",\"price\":225.0}");
	}
	
	@Test
	public void deleteBook() throws Exception {
		String uri = "/books/delete/7";
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.delete(uri)).andReturn();
		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
		String content = mvcResult.getResponse().getContentAsString();
		assertEquals(content, "Book is deleted successsfully");
	}
	@Test
	public void updateBook() throws Exception {
		String uri = "/books/update";
		Book Book = new Book(new Long(10001), "The Monk who sold his ferrari", "Robin Shanrma", "Fiction", 225.00);
		String inputJson = super.mapToJson(Book);
		MvcResult mvcResult = mvc.perform(
				MockMvcRequestBuilders.put(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
				.andReturn();

		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
		String content = mvcResult.getResponse().getContentAsString();
		assertEquals(content, "Book is updated successsfully");
	}

}
