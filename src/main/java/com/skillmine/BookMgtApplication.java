package com.skillmine;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.skillmine.model.Book;
import com.skillmine.repository.BookRepository;

@SpringBootApplication
public class BookMgtApplication implements CommandLineRunner {
	 private static final Logger logger = LoggerFactory.getLogger(BookMgtApplication.class);
	@Autowired
	private BookRepository bookRepository;
	
	public static void main(String[] args) {
		SpringApplication.run(BookMgtApplication.class, args);
	}
	@Override
	public void run(String... args) throws Exception {
		bookRepository.save(new Book(new Long(10001),"The Monk who sold his ferrari", "Robin Shanrma","Fiction",225.00));
		bookRepository.save(new Book(new Long(10002),"Inner Engineering", "Sadguru","Spritual",199.00));
		bookRepository.save(new Book(new Long(10003),"Miracle Morning", "Miracle Morning","Non Fiction",123.45));
		bookRepository.save(new Book(new Long(10004),"The Power od subconscious Mind", "Joseph M","Fiction",250));
		bookRepository.save(new Book(new Long(10005),"The Mastry Manual", "Robin Sharma","Non Fiction",250.50));

        logger.info("# of Books: {}", bookRepository.count());

        logger.info("All Books unsorted:");
        List<Book> books = bookRepository.findAll();
        logger.info("{}", books);

        logger.info("------------------------");
		
	}
}
