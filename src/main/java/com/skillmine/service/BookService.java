package com.skillmine.service;

import java.util.List;
import java.util.Optional;

import com.skillmine.model.Book;

public interface BookService {
	
	public List<Book> getBooks();
	public Optional<Book> getBook(Long id);
	public Book createBook(Book book);
	public Optional<Book>  getBookById(Long id);
	public void deleteBookById(Long id);
	public Book updateBook(Book book);
	public List<Book> search(String title);
	

}
