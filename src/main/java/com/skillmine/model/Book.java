package com.skillmine.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;

import lombok.ToString;

@Entity
@Indexed
@ToString
public class Book implements Serializable{
	/**
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
    private Long isbn;
	@Field
	private String title;
	@Field(name="auther")
	private String auther;
	@Field(name="type")
	private String type;
	private double price;
	
	public Book() {
		super();
	}
	public Book(Long isbn, String title, String auther, String type, double price) {
		super();
		this.isbn = isbn;
		this.title = title;
		this.auther = auther;
		this.type = type;
		this.price = price;
	}
	public Long getIsbn() {
		return isbn;
	}
	public void setIsbn(Long isbn) {
		this.isbn = isbn;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getAuther() {
		return auther;
	}
	public void setAuther(String autherName) {
		this.auther= autherName;
	}
	public String getType() {
		return type;
	}
	public void setType(String bookType) {
		this.type = bookType;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
}
