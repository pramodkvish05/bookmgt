package com.skillmine.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.skillmine.model.Book;
import com.skillmine.serviceimpl.BookServiceImpl;
@RestController
@RequestMapping("books")
public class BookController {

	@Autowired
	private BookServiceImpl bookService;
	@GetMapping("getAll")
	public List<Book> getBooks() {
		return bookService.getBooks();
		
	}
	@GetMapping("search/{term}")
	public List<Book> search(@PathVariable(name="term") String title) {
		System.out.println("BookController.search()"+title);
		return bookService.search(title);
		
	}
	@GetMapping("getBook")
	public Book getBook(@RequestParam String id) {
		return bookService.getBookById(new Long(id)).get();
	}
	@PostMapping("create")
	public Book CreateBook(@RequestBody Book book) {
		return bookService.createBook(book);
		
	}
	@DeleteMapping("delete/{id}")
	public String deleteBook(@PathVariable(name="id") Long id) {
		 bookService.deleteBookById(id);
		return "Book is deleted successsfully";
		
	}
	@PutMapping("update")
	public String updateBook(@Valid Book book) {
		 bookService.updateBook(book);
		return "Book is updated successsfully";
		
	}
}
