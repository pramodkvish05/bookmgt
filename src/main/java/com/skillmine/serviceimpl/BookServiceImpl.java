package com.skillmine.serviceimpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.skillmine.model.Book;
import com.skillmine.repository.BookRepository;
import com.skillmine.service.BookService;
@Service
public class BookServiceImpl implements BookService{
 
	@Autowired
    private HibernateSearchService searchservice;
	@Autowired
	private BookRepository bookRepository; 
	@Override
	public List<Book> getBooks() {
		return bookRepository.findAll();
	}

	@Override
	public Optional<Book> getBook(Long id) {
		return bookRepository.findById(id);
	}

	@Override
	public Book createBook(Book book) {
		return bookRepository.save(book);
	}

	@Override
	public Optional<Book> getBookById(Long id) {
		return bookRepository.findById(id);
	}

	@Override
	public void deleteBookById(Long id) {
		 bookRepository.deleteById(id);
	}

	@Override
	public Book updateBook(Book book) {
		return bookRepository.save(book);
	}

	@Override
	public List<Book> search(String title) {
		return searchservice.fuzzySearch(title);
	}
}
