package com.skillmine.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.skillmine.model.Book;

@Repository
public interface BookRepository extends  JpaRepository<Book, Long>  {
	@Query(value = "SELECT book.* FROM Books book WHERE MATCH (book.title, book.auther_name) AGAINST (:title IN NATURAL LANGUAGE MODE)", nativeQuery = true)
    public List<Book> findFullTextSearchByTitle(@Param("title") String title);
}
