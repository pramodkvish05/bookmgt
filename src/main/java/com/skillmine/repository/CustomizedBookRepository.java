package com.skillmine.repository;

import java.util.List;

import com.skillmine.model.Book;

public interface CustomizedBookRepository {
	List<Book> search(String terms, int limit, int offset);
}
