CREATE TABLE Books (  
isbn NUMBER  PRIMARY KEY,  
title VARCHAR(50)  NOT NULL,  
auther_name VARCHAR(8)  NULL,
book_type VARCHAR(10)  NULL,
price NUMBER(5,2)
);  